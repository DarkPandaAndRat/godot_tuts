extends Node 

#declaring variables globally

#global constant - cannot be changed anywhere in the code
const pi = 3.14

#exportnig variable out to editor - can be useful 
export var radius = 2
export(Texture) var myImage


var a = 13				#int
var b = 13.13			#float
var c = 'thirteen'		#string
var d = Vector2(1,0)	#vector
var e = String(b) + c	#converting + adding type


# Called when the node enters the scene tree for the first time.
func _ready():
	var zeroToNine = Array()
	for i in range(10):
		zeroToNine.append(i)
		print(i)
		
	#declaring variable locally
	var inside_var = 99
	
	print(zeroToNine)
	
	pass # Replace with function body.

	var area = pi * radius
	print(area)
	
	var myArray = [1,2,3,4,5.55,"lol"]
	print(myArray)
	myArray.append("Bob")
	print(myArray)
	for i in myArray:
		print(i)
		
	#dict - key value
	var monster = {}
	monster['Name'] = 'Orc'
	print(monster.Name)
	
#########################################
	#different way of defining dictionary
	var monster2 = {
		"Orc" : {
			"hp" : 42,
			"Alignment" : "Chaotic Neutral"
		},
		"Elf" : {
			"hp" : 12,
			"Alignment" : "Neutral"
		}
	}
	
	#if
	if (monster2.Elf.hp < 10):
		print("Elf need health potion! ASAP")
	elif (monster2.Elf.hp == 11):
		print("Elf wouldn't mind some food")
	else:
		print("Elf Healthy!!!")
#########################################	

	#switch
	var val = 6
	match GameGlobals.meaningoflife:
		1: print("value is 1")
		2,3,4,5: print("value is between 2 - 5")
		#if it 6 - continue
		6:
			print("it is 6")
			continue #do something here
		#this is 'catch 'all' - in case of val get < 0 or > 6
		42:
			print("meaning of life is 42")
		_: print("Something else")
		
