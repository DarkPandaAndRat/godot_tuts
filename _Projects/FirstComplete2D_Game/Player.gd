#THIS IS PLAYER SCRIPT!
extends Area2D											#inheritance of properties from object: Area2D
export var speed = 400.0								#this will create new item in inspector menu
var screen_size = Vector2.ZERO	#

func _ready():
	screen_size = get_viewport_rect().size
	print(screen_size)
	
	
func _process(delta):									#how character updates every frame
	var direction = Vector2.ZERO						#Vector2.ZERO = Vector(0, 0)
	
	if Input.is_action_pressed("move_right"):
		direction.x += 1
	if Input.is_action_pressed("move_left"):
		direction.x -= 1
		
	if Input.is_action_pressed("move_down"):
		direction.y += 1		
	if Input.is_action_pressed("move_up"):
		direction.y -= 1

	if direction.length() > 0:
		direction = direction.normalized()
		$AnimatedSprite.play()							#get_node("AnimatedSprite").play() < another method to get node
	else:
		$AnimatedSprite.stop()	


	position += direction * speed * delta				#position definition - updates every frame: += direction * speed * delta	
	
	position.x = clamp(position.x, 0, screen_size.x)	#position.x - max position of player on X-Axis: from 0 to screen_size.x
	position.y = clamp(position.y, 0, screen_size.y)	#position.y - max position of player on Y-Axis: from 0 to screen_size.y
	
	if direction.x != 0:
		$AnimatedSprite.animation = "right"
		$AnimatedSprite.flip_h = direction.x < 0
		$AnimatedSprite.flip_v = false
	elif direction.y != 0:
		$AnimatedSprite.animation = "up"
		$AnimatedSprite.flip_v = direction.y > 0
		#$AnimatedSprite.flip_h = false
	
